use std::fs::File;
use std::io::prelude::*;
use std::iter::Peekable;
use std::str::Chars;

pub type Perms = u8;

pub const PERM_READ: Perms = 1;
pub const PERM_WRITE: Perms = 1 << 1;
pub const PERM_EXEC: Perms = 1 << 2;

#[derive(Debug)]
pub struct FilePos {
    pub filename: String,
    pub position: usize,
}

#[derive(Debug, Clone)]
pub struct FileMaps {
    entries: Vec<Entry>,
}

impl FileMaps {
    pub fn from_lines(input: &str) -> Self {
        let mut entries = Vec::new();

        for line in input.lines() {
            entries.push(Entry::from_line(line));
        }

        Self::from_entries(entries)
    }

    pub fn from_process() -> Self {
        let mut file = File::open("/proc/self/maps").unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        Self::from_lines(&contents[..])
    }

    pub fn from_entries(entries: Vec<Entry>) -> Self {
        FileMaps { entries }
    }

    pub fn position_for_addr(&self, addr: usize) -> Option<FilePos> {
        for ent in &self.entries {
            if ent.begin <= addr && addr <= ent.end {
                let pos = addr - ent.begin + ent.offset;
                return Some(FilePos {
                    filename: ent.pathname.clone(),
                    position: pos,
                });
            }
        }

        None
    }
}

#[derive(Debug, Clone)]
pub struct Entry {
    pub begin: usize,
    pub end: usize,
    pub perms: Perms,
    pub offset: usize,
    pub dev: (u8, u8),
    pub inode: usize,
    pub pathname: String,
}

impl Entry {
    pub fn from_line(input: &str) -> Self {
        let mut et = EntryParser::new(input);

        let (begin, end) = et.parse_addr();
        let perms = et.parse_perms();
        let offset = et.parse_off();
        let dev = et.parse_dev();
        let inode = et.parse_inode();
        let pathname = et.parse_path();

        Entry {
            begin,
            end,
            perms,
            offset,
            dev,
            inode,
            pathname,
        }
    }
}

struct EntryParser<'a> {
    input: Peekable<Chars<'a>>,
}

impl<'a> EntryParser<'a> {
    fn new(input: &'a str) -> Self {
        EntryParser {
            input: input.chars().peekable(),
        }
    }

    fn parse_addr(&mut self) -> (usize, usize) {
        let mut begin = String::new();
        let mut end = String::new();

        loop {
            match self.input.next() {
                Some(c) if c.is_digit(16) => {
                    begin.push(c);
                }
                _ => break, // hyphen
            }
        }

        for _ in 0..begin.len() {
            end.push(self.input.next().unwrap());
        }

        let begin = usize::from_str_radix(&begin, 16).unwrap();
        let end = usize::from_str_radix(&end, 16).unwrap();

        (begin, end)
    }

    fn spaces(&mut self) {
        while let Some(c) = self.input.peek() {
            if *c == ' ' || *c == '\t' {
                self.input.next();
            } else {
                break;
            }
        }
    }

    fn parse_perms(&mut self) -> Perms {
        self.spaces();

        let mut perms = 0;

        for _ in 0..4 {
            match self.input.next().unwrap() {
                'r' => {
                    perms |= PERM_READ;
                }
                'w' => {
                    perms |= PERM_WRITE;
                }
                'x' => {
                    perms |= PERM_EXEC;
                }
                _ => (),
            }
        }

        perms
    }

    fn parse_off(&mut self) -> usize {
        self.spaces();

        let mut off = String::new();

        loop {
            match self.input.next() {
                Some(c) if c.is_digit(16) => {
                    off.push(c);
                }
                _ => break, // space
            }
        }

        usize::from_str_radix(&off, 16).unwrap()
    }

    fn parse_dev(&mut self) -> (u8, u8) {
        self.spaces();

        let m = format!(
            "{}{}",
            self.input.next().unwrap(),
            self.input.next().unwrap()
        );
        self.input.next(); // colon
        let n = format!(
            "{}{}",
            self.input.next().unwrap(),
            self.input.next().unwrap()
        );

        (
            u8::from_str_radix(&m, 16).unwrap(),
            u8::from_str_radix(&n, 16).unwrap(),
        )
    }

    fn parse_inode(&mut self) -> usize {
        self.spaces();

        let mut inode = String::new();

        loop {
            match self.input.next() {
                Some(c) if c.is_digit(10) => {
                    inode.push(c);
                }
                _ => break, // space
            }
        }

        inode.parse().unwrap()
    }

    fn parse_path(&mut self) -> String {
        self.spaces();

        let mut res = String::new();

        for c in self.input.clone() {
            res.push(c);
        }

        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let input = "85c03000-85cc8000 r-xp 00000000 fd:00 1210116    /data/data/com.termux/files/usr/bin/coreutils";
        let data = Entry::from_line(input);
        assert_eq!(data.begin, 0x85c03000);
        assert_eq!(data.end, 0x85cc8000);
        assert_eq!(data.perms, PERM_READ | PERM_EXEC);
        assert_eq!(data.offset, 0);
        assert_eq!(data.dev, (0xfd, 0x00));
        assert_eq!(data.inode, 1210116);
        assert_eq!(
            data.pathname,
            "/data/data/com.termux/files/usr/bin/coreutils"
        );
    }
}
